package com.teg.storelocator.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.teg.storelocator.beans.Ziplocation;

@Repository
public interface ZipLocationRepository extends MongoRepository<Ziplocation, String> {

    public Ziplocation findOneByZipcode(String zipcode);

}