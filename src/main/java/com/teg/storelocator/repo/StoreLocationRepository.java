package com.teg.storelocator.repo;

import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.teg.storelocator.beans.StoreLocation;

@Repository
public interface StoreLocationRepository extends MongoRepository<StoreLocation, String> {

	public List<StoreLocation> findByZipcode(String zipcode);
	public List<StoreLocation> findByState(String state);
	public GeoResults<StoreLocation> findByLocationNear(Point point,Distance distance);

}
