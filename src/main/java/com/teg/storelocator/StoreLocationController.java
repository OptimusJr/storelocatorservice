package com.teg.storelocator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teg.storelocator.beans.StoreLocation;
import com.teg.storelocator.beans.Ziplocation;
import com.teg.storelocator.repo.StoreLocationRepository;
import com.teg.storelocator.repo.ZipLocationRepository;

@RestController
@ComponentScan
@RequestMapping(value = "/store")
public class StoreLocationController {

	@Autowired
	StoreLocationRepository storeLocationRepository;

	@Autowired
	ZipLocationRepository zipLocationRepository;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody StoreLocation StoreLocation) {
		storeLocationRepository.save(StoreLocation);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<StoreLocation> read() {
		return storeLocationRepository.findAll();
	}

	@RequestMapping(value = "/byzipcode/{zipcode}/{distance}", method = RequestMethod.GET)
	public List<StoreLocation> readbyZipCode(@PathVariable String zipcode, @PathVariable String distance) {

		Ziplocation ziplocation = zipLocationRepository.findOneByZipcode(zipcode);
		List ls = new ArrayList<StoreLocation>();
		Double lattitude = Double.valueOf(ziplocation.getCoordinates()[0]);
		Double longitude = Double.valueOf(ziplocation.getCoordinates()[1]);

		Point point = new Point(longitude, lattitude);
		Distance distant = new Distance(Double.valueOf(distance), Metrics.MILES);
		GeoResults<StoreLocation> results = storeLocationRepository.findByLocationNear(point, distant);
		results.forEach((k) -> {
			ls.add(k);
		});

		// TODO COnverting Georesults json to standard format
		return ls;
	}

	@RequestMapping(value = "/bystate/{state}", method = RequestMethod.GET)
	public List<StoreLocation> readByState(@PathVariable String state) {
		return storeLocationRepository.findByState(state);
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void update(@RequestBody StoreLocation StoreLocation) {
		storeLocationRepository.save(StoreLocation);
	}

	@RequestMapping(value = "/bypoint/{longitude}/{lattitude}", method = RequestMethod.GET)
	public GeoResults<StoreLocation> getStoreLocation(@PathVariable String longitude, @PathVariable String lattitude) {
		System.out.println("-------------------------------" + lattitude + "::" + longitude);
		Float lat = Float.valueOf(lattitude.replace("_", "."));
		Float lng = Float.valueOf(longitude.replace("_", "."));
		System.out.println("-------------------------------" + lat + "::" + lng);
		Point point = new Point(lng, lat);
		Distance distance = new Distance(25, Metrics.MILES);
		GeoResults<StoreLocation> results = storeLocationRepository.findByLocationNear(point, distance);
		return results;

	}

}
