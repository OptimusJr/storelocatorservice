package com.teg.storelocator;

import java.util.HashMap;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
 

@SpringBootApplication
@ComponentScan({"com.teg"})
public class StoreApplication {

	public static void main(String[] args) {
		   HashMap<String, Object> props = new HashMap<>();
		    props.put("server.port", 8051);
		 new SpringApplicationBuilder()
	        .sources(StoreApplication.class).properties(props)              
	        .run(args);

	}

}
