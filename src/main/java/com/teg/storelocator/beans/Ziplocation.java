package com.teg.storelocator.beans;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ziplocation")
public class Ziplocation {
	@Id
	private String _id;

	private String zipcode;

	private String[] coordinates;

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String[] coordinates) {
		this.coordinates = coordinates;
	}

	@Override
	public String toString() {
		return "ClassPojo [zipcode = " + zipcode + ", coordinates = " + coordinates + "]";
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
}
