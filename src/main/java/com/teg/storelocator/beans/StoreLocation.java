package com.teg.storelocator.beans;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="storelocation")
public class StoreLocation {
	 
	    private String phone;

	    private String fax;

	    private String store;

	    private Location location;

	    private String zipcode;

	    private String state;

	    private String address1;

	    private String address2;

	    private String address3;

	    private String active_hours;

	    private String city;

	    private String country;

	    private String landmark;

	    @Id
	    private String _id;

	    private String latitude;

	    private String active;

	    private String longitude;

	    private String identifier;

	    public String getPhone ()
	    {
	        return phone;
	    }

	    public void setPhone (String phone)
	    {
	        this.phone = phone;
	    }

	    public String getFax ()
	    {
	        return fax;
	    }

	    public void setFax (String fax)
	    {
	        this.fax = fax;
	    }

	    public String getStore ()
	    {
	        return store;
	    }

	    public void setStore (String store)
	    {
	        this.store = store;
	    }

	    public Location getLocation ()
	    {
	        return location;
	    }

	    public void setLocation (Location location)
	    {
	        this.location = location;
	    }

	    public String getZipcode ()
	    {
	        return zipcode;
	    }

	    public void setZipcode (String zipcode)
	    {
	        this.zipcode = zipcode;
	    }

	    public String getState ()
	    {
	        return state;
	    }

	    public void setState (String state)
	    {
	        this.state = state;
	    }

	    public String getAddress1 ()
	    {
	        return address1;
	    }

	    public void setAddress1 (String address1)
	    {
	        this.address1 = address1;
	    }

	    public String getAddress2 ()
	    {
	        return address2;
	    }

	    public void setAddress2 (String address2)
	    {
	        this.address2 = address2;
	    }

	    public String getAddress3 ()
	    {
	        return address3;
	    }

	    public void setAddress3 (String address3)
	    {
	        this.address3 = address3;
	    }

	    public String getActive_HOURS ()
	    {
	        return active_hours;
	    }

	    public void setActive_HOURS (String active_HOURS)
	    {
	        this.active_hours = active_HOURS;
	    }

	    public String getCity ()
	    {
	        return city;
	    }

	    public void setCity (String city)
	    {
	        this.city = city;
	    }

	    public String getCountry ()
	    {
	        return country;
	    }

	    public void setCountry (String country)
	    {
	        this.country = country;
	    }

	    public String getLandmark ()
	    {
	        return landmark;
	    }

	    public void setLandmark (String landmark)
	    {
	        this.landmark = landmark;
	    }

	    public String get_id ()
	    {
	        return _id;
	    }

	    public void set_id (String _id)
	    {
	        this._id = _id;
	    }

	    public String getLattitude ()
	    {
	        return latitude;
	    }

	    public void setLattitude (String lattitude)
	    {
	        this.latitude = lattitude;
	    }

	    public String getActive ()
	    {
	        return active;
	    }

	    public void setActive (String active)
	    {
	        this.active = active;
	    }

	    public String getLongitude ()
	    {
	        return longitude;
	    }

	    public void setLongitude (String longitude)
	    {
	        this.longitude = longitude;
	    }

	    public String getIdentifier ()
	    {
	        return identifier;
	    }

	    public void setIdentifier (String identifier)
	    {
	        this.identifier = identifier;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [phone = "+phone+", fax = "+fax+", store = "+store+", location = "+location+", zipcode = "+zipcode+", state = "+state+", address1 = "+address1+", address2 = "+address2+", address3 = "+address3+", active_HOURS = "+active_hours+", city = "+city+", country = "+country+", landmark = "+landmark+", _id = "+_id+", lattitude = "+latitude+", active = "+active+", longitude = "+longitude+", identifier = "+identifier+"]";
	    }
	}

